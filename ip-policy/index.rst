.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

Intellectual Property Compliance Policy
#######################################

.. toctree::
   :maxdepth: 1

   ip-policy_policy/index
   ip-policy_implementation-guidelines/index
